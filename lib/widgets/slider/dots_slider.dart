import 'package:flutter/material.dart';

class DortsSlider extends StatelessWidget {
  final bool isActive;
  final Color? activeColor;
  final Color? inactiveColor;
  final double? vPadding;
  final double? hPadding;

  DortsSlider(
      {Key? key,
      required this.isActive,
      this.activeColor,
      this.inactiveColor,
      this.vPadding,
      this.hPadding})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: vPadding ?? 8, horizontal: hPadding ?? 4),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        height: isActive ? 15 : 10,
        width: isActive ? 15 : 10,
        decoration: BoxDecoration(
            color: isActive
                ? activeColor ?? Colors.blue
                : inactiveColor ?? Colors.grey,
            borderRadius: BorderRadius.circular(50)),
      ),
    );
  }
}
