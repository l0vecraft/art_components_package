import 'dart:async';

import 'package:art_components_package/widgets/slider/dots_slider.dart';
import 'package:flutter/material.dart';

class ArtSlider extends StatefulWidget {
  final Color? dotsColor;
  final List<Widget> childrens;

  const ArtSlider({
    Key? key,
    this.dotsColor,
    required this.childrens,
  }) : super(key: key);
  @override
  _ArtSliderState createState() => _ArtSliderState();
}

class _ArtSliderState extends State<ArtSlider> {
  PageController? _controller;

  int _currentPage = 0;

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: 0, viewportFraction: .9);
    Timer.periodic(Duration(seconds: 3), (Timer time) {
      if (_currentPage < widget.childrens.length) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
      _controller!.animateToPage(_currentPage,
          duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * .97,
      height: size.height * .3,
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          PageView.builder(
            itemCount: widget.childrens.length,
            controller: _controller,
            onPageChanged: (index) {
              setState(() {
                _currentPage = index;
              });
            },
            itemBuilder: (context, index) => _currentPage == index
                ? widget.childrens[index]
                : Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: widget.childrens[index],
                  ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(
                widget.childrens.length,
                (index) => DortsSlider(
                      isActive: index == _currentPage,
                      activeColor: widget.dotsColor,
                    )).toList(),
          ),
        ],
      ),
    );
  }
}
