import 'package:flutter/material.dart';

class ArtUserMessage extends StatelessWidget {
  final Color? color;
  final String text;
  final bool hasGradient;
  final double? topRound;
  final double? bottomRound;
  final TextStyle? textStyle;
  final List<Color>? colors;

  const ArtUserMessage({
    Key? key,
    this.color,
    required this.text,
    this.hasGradient = false,
    this.topRound,
    this.bottomRound,
    this.textStyle,
    this.colors,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
      constraints: BoxConstraints(
        maxWidth: 220,
      ),
      decoration: BoxDecoration(
          color: hasGradient ? null : color ?? Colors.blue,
          gradient: hasGradient
              ? LinearGradient(
                  colors: colors!,
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)
              : null,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(topRound ?? 10),
              topRight: Radius.circular(topRound ?? 10),
              bottomLeft: Radius.circular(bottomRound ?? 10))),
      child: Text(
        text,
        style: textStyle,
      ),
    );
  }
}
