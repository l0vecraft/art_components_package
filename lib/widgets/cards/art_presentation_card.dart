import 'package:flutter/material.dart';

class ArtPresentationCard extends StatelessWidget {
  final String image;
  final String text;
  final Function? onPress;

  const ArtPresentationCard(
      {Key? key, required this.image, required this.text, this.onPress})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: size.height * .01),
      child: Container(
        height: size.height * .16,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(image),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(.2), BlendMode.darken)),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Text(
                text,
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: 22,
                    color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
