import 'package:flutter/material.dart';

class ArtDescriptionCard extends StatelessWidget {
  final String title;
  final String? subtitle;
  final String image;
  final double? stars;
  final Function? onPress;
  final Color? titleColor;
  final Color? subtitleColor;
  final Color? starsColor;

  const ArtDescriptionCard({
    Key? key,
    required this.title,
    this.subtitle,
    required this.image,
    this.stars,
    this.onPress,
    this.titleColor,
    this.subtitleColor,
    this.starsColor,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onPress as void Function()?,
      child: Container(
        height: size.height * .35,
        width: size.width * .55,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(image),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: size.height * .007, horizontal: size.width * .02),
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: 18,
                    color: titleColor ?? Colors.black),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: size.width * .02),
              child: Text(
                subtitle ?? '',
                style: TextStyle(color: titleColor ?? Colors.grey),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
