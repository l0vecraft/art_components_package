import 'package:flutter/material.dart';

class ArtCategoryCard extends StatelessWidget {
  final String image;
  final String? text;
  final Function? onPress;
  final TextStyle? textStyle;

  ArtCategoryCard({
    Key? key,
    required this.image,
    this.text,
    this.onPress,
    this.textStyle,
  }) : super(key: key);

  final BorderRadius _borderRadius = BorderRadius.only(
      topLeft: Radius.circular(20),
      topRight: Radius.circular(20),
      bottomLeft: Radius.circular(5),
      bottomRight: Radius.circular(5));
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onPress as void Function()?,
      borderRadius: _borderRadius,
      child: Container(
        height: size.height * .18,
        width: size.width * .42,
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: _borderRadius),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                child: Image.network(
                  image,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: size.height * .005),
                child: Text(
                  text ?? '',
                  style: textStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
