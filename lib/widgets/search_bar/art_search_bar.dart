import 'package:flutter/material.dart';

class ArtSearchBar extends StatefulWidget {
  final double? height;
  final double? width;
  final Color? color;
  final Color? cursorColor;
  final Function? onSearch;

  ArtSearchBar({
    Key? key,
    this.height,
    this.width,
    this.color,
    this.cursorColor,
    this.onSearch,
  }) : super(key: key);

  @override
  _ArtSearchBarState createState() => _ArtSearchBarState();
}

class _ArtSearchBarState extends State<ArtSearchBar> {
  TextEditingController? _text;

  @override
  void initState() {
    super.initState();
    _text = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: size.height * .01, horizontal: size.width * .04),
      child: Container(
        height: size.height * .05,
        decoration: BoxDecoration(
            color: widget.color ?? Colors.grey.shade300,
            borderRadius: BorderRadius.circular(24)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(Icons.search),
              onPressed: widget.onSearch as void Function()?,
            ),
            Expanded(
              child: TextField(
                controller: _text,
                onSubmitted: (value) {
                  setState(() {
                    _text!.text = value;
                  });
                },
                cursorColor: widget.cursorColor ?? Colors.blue,
                decoration: InputDecoration(
                    hintText: 'Search...',
                    isCollapsed: true,
                    focusColor: Colors.transparent),
              ),
            ),
            IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                _text!.clear();
              },
            ),
          ],
        ),
      ),
    );
  }
}
