import 'package:flutter/material.dart';

class ArtButton extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final VoidCallback onPress;
  final bool hasGradient;
  final bool hasBorder;
  final Color? borderColor;
  final double? height;
  final double? width;
  final double? borderWidth;
  final List<Color>? colors;
  final Color? color;
  final bool hasIcon;
  final IconData? icon;
  final double? radius;

  const ArtButton({
    Key? key,
    required this.text,
    this.style,
    required this.onPress,
    this.hasGradient = false,
    this.height,
    this.width,
    this.colors,
    this.color,
    this.hasIcon = false,
    this.icon,
    this.radius,
    this.hasBorder = false,
    this.borderColor,
    this.borderWidth,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onPress ?? () {},
        child: Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
            color: color ?? Colors.blue,
            gradient: hasGradient
                ? LinearGradient(
                    colors: colors!,
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)
                : null,
            borderRadius: BorderRadius.circular(radius!),
            border: hasBorder
                ? Border.all(color: borderColor!, width: borderWidth!)
                : null,
          ),
          child: Center(
            child: Text(
              text,
              style: style,
            ),
          ),
        ));
  }
}
