import 'package:flutter/material.dart';

class ArtProfileTemplate extends StatelessWidget {
  final Widget? appBar;
  final String? image;
  final String logOutText;
  final double fontSize;
  final double radius;
  final List<Widget> items;
  final IconData icon;
  final Color colorIcons;
  final Color backgrounIconColor;
  final VoidCallback onPress;
  final VoidCallback onExit;

  const ArtProfileTemplate({
    Key? key,
    this.appBar,
    this.image,
    required this.items,
    this.radius = 60,
    this.icon = Icons.camera_alt,
    this.colorIcons = Colors.white,
    this.backgrounIconColor = Colors.blue,
    required this.onPress,
    required this.onExit,
    this.logOutText = "Log out",
    this.fontSize = 22,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: appBar as PreferredSizeWidget? ?? null,
        body: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Padding(
                  padding: EdgeInsets.symmetric(vertical: size.height * .1),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      CircleAvatar(
                        radius: radius,
                        backgroundColor: Colors.redAccent,
                        child: image != null
                            ? Image.network(image!)
                            : Text('image'),
                      ),
                      Positioned(
                        right: 0,
                        bottom: 0,
                        child: CircleAvatar(
                          radius: 23,
                          backgroundColor: backgrounIconColor,
                          child: IconButton(
                            icon: Icon(
                              icon,
                              size: 22,
                              color: colorIcons,
                            ),
                            onPressed: onPress,
                          ),
                        ),
                      ),
                    ],
                  )),
              Expanded(
                child: ListView.builder(
                    padding: EdgeInsets.only(left: size.width * .03),
                    itemCount: items.length,
                    itemBuilder: (context, i) => items[i]),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: size.height * .08),
                child: FlatButton(
                    onPressed: () {},
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: size.width * .2,
                          vertical: size.height * .015),
                      child: Text(logOutText,
                          style: TextStyle(
                              fontWeight: FontWeight.w800, fontSize: fontSize)),
                    )),
              ),
            ],
          ),
        ));
  }
}
