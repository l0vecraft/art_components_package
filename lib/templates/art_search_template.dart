import 'package:art_components_package/widgets/search_bar/art_search_bar.dart';
import 'package:flutter/material.dart';

class ArtSearchTemplate extends StatefulWidget {
  final String? title;
  final Color? titleColor;
  final Color? cursorColor;
  final Widget? avatar;
  final Color appBarColor;
  final List<String> results;

  const ArtSearchTemplate({
    Key? key,
    this.title,
    this.avatar,
    this.appBarColor = Colors.white,
    this.titleColor,
    required this.results,
    this.cursorColor,
  }) : super(key: key);

  @override
  _ArtSearchTemplateState createState() => _ArtSearchTemplateState();
}

class _ArtSearchTemplateState extends State<ArtSearchTemplate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: widget.avatar != null ? [widget.avatar!] : [],
        title:
            Text('${widget.title}', style: TextStyle(color: widget.titleColor)),
        centerTitle: true,
        backgroundColor: widget.appBarColor,
      ),
      body: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ArtSearchBar(
            cursorColor: widget.cursorColor,
            onSearch: () {
              showSearch(
                  context: context,
                  delegate: Search(listResult: widget.results));
            },
          )
        ],
      )),
    );
  }
}

class Search extends SearchDelegate {
  final List<String>? listResult;

  Search({this.listResult});
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    late String result;

    /// page tha will override the main screen
    // TODO: pedir un widget
    return Container(
      child: Center(
        child: Text(result),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return ListView.builder(
        itemCount: listResult!.length,
        itemBuilder: (context, i) => ListTile(
              title: Text(listResult![i]),
            ));
  }
}
